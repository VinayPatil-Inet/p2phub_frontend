

import React, { Component, useState, useMemo, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { Form, Button, Container, custom, Card, Row, Col, Alert } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';
import { MDBInput } from "mdbreact";
import './css/custom.css'; //Import here your file style


const Signup = () => {

    //form validation rules 
    const validationSchema = Yup.object().shape({

        password: Yup.string()
            .required('Password is required'),
        confirm_password: Yup.string()
            .required('Confirm Password is required')
            .oneOf([Yup.ref('password')], 'Passwords must match')

    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, trigger, formState } = useForm();
    const { errors } = formState;
    const [errorMessage, setErrorMessage] = React.useState("");
    const [successMessage, setSuccessMessage] = React.useState("");
    const [usertypeList, setUserTypeList] = useState([]);

    const onSubmit = data => {
     //   console.log(data, "data")
        axios

            .post(
                'http://localhost:7000/api/user/create',
                data,
            )
            .then(response => {
                setSuccessMessage("Successfully created a user!")
                reset(response.data);
                setTimeout(() => {
                    setSuccessMessage()
                }, 2000);
                console.log(response.data, "ppppppppppp")
            })
            .catch(error => {
                setErrorMessage("Cannot created user, "+error.response.data.body)
               
                setTimeout(() => {
                    setErrorMessage()
                }, 2000);
                console.log(error.response.data.body, "body")
              
                console.log(error.response, "data")
            });
    };
    useEffect(() => {
        getUserTypesList();
    }, []);

    function getUserTypesList() {
        axios.get('http://localhost:7000/api/getOrganisationTypes').then(res => {
            setUserTypeList(res.data.data);
            console.log(res.data.data, "pppppppppp")
        });
    }

    return (
        <div class="signup_header">
            <div style={{ width: '60%', marginLeft: '20%' }}>
                <Card>
                    <Card.Body>
                        <ul id="progressbar">
                            {/* <li class="active">Organization Information</li> */}
                            {/* <li>Administrator Information</li>
                            <li>Review and  Confirm</li> */}
                        </ul>
                        <h3 className='text-center mb-4 heading2'>Register User</h3>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Row md={12} style={{ width: "100%", }}>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.organisation_name">
                                        <Form.Label>Organisation Name  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("organisation_name", { required: true })}
                                        />
                                      {/* {errors.organisation_name && errors.organisation_name.type === "required" &&
                                          <h4 style={{color:"red",marginRight:"90%"}}>*</h4>} */}

                                    </Form.Group>
                                </Col>
                                <Col md={6} >

                                    <Form.Group className="mb-3" controlId="pcform.organisation_type_id">
                                        <Form.Label>Organisation Type  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <br />
                                        <select className="form-control" name="payer"

                                            {...register("organisation_type_id", {
                                                required: true,
                                                validate: (value) => value !== "Select Organisation Type Id"
                                            })}
                                        >
                                            <option>Select  Organisation Type</option>
                                            {
                                                usertypeList.map((result) => (<option id={result.id} value={result.id}> {result.organisation_type}</option>
                                                ))
                                            }
                                        </select>
                                        {/* {errors.organisation_type_id && <p style={{ color: "red" }}>Please select at least one option</p>} */}

                                    </Form.Group>

                                </Col>
                            </Row>
                            <Row md={12} style={{ width: "100%", }}>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.email">
                                        <Form.Label>Email  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("email", { required: true, pattern: /^\S+@\S+$/i })}
                                        />
                                      {/* {errors.email && errors.email.type === "required" &&
                                             <h4 style={{color:"red",marginRight:"90%"}}>*</h4>}
                                        {errors.email && errors.email.type === "pattern" && <p style={{ color: "red" }}>Invalid Email</p>}  */}
                                    </Form.Group>
                                </Col>
                           
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.password">
                                        <Form.Label>Password  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="password"
                                            {...register("password", { required: true, maxLength: 60 })}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row md={12} style={{ width: "100%", }}>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.first_name">
                                        <Form.Label> First Name  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("first_name", { required: true })}
                                        />
                                        {/* {errors.first_name && errors.first_name.type === "required" &&
                                            <p style={{ color: "red" }}> First Name is required</p>} */}
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.last_name">
                                        <Form.Label> Last Name  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("last_name", { required: true })}
                                        />
                                        {/* {errors.last_name && errors.last_name.type === "required" &&
                                            <p style={{ color: "red" }}> Last Name is required</p>} */}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row md={12} style={{ width: "100%", }}>
                                <Col md={6}>
                                    <Form.Group className="mb-3 addr" controlId="pcform.address1">
                                        <Form.Label>Address Line 1  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("address1", { required: true })}
                                        />
                                        {/* {errors.address1 && errors.address1.type === "required" &&
                                            <p style={{ color: "red" }}>Address1 is required</p>} */}
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group className="mb-3 addr" controlId="pcform.address2">
                                        <Form.Label>Address Line 2</Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("address2", { required: true })}
                                        />
                                        {/* {errors.address2 && errors.address2.type === "required" &&
                                            <p style={{ color: "red" }}>Address2 is required</p>} */}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row md={12} style={{ width: "100%", }}>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.city">
                                        <Form.Label>City  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("city", { required: true, maxLength: 60 })}
                                        />
                                        {/* {errors.city && errors.city.type === "required" &&
                                            <p style={{ color: "red" }}>City is required</p>} */}
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.country">
                                        <Form.Label>State  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("state", { required: true, maxLength: 60 })}
                                        />
                                        {/* {errors.state && errors.state.type === "required" &&
                                            <p style={{ color: "red" }}>State is required</p>} */}
                                    </Form.Group>
                                </Col>
                               
                            </Row>

                            <Row md={12} style={{ width: "100%", }}>
                            <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.email">
                                        <Form.Label>Country  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("country", { required: true, maxLength: 60 })}
                                        />
                                        {/* {errors.country && errors.country.type === "required" &&
                                            <p style={{ color: "red" }}>Country is required</p>} */}
                                    </Form.Group>
                                </Col>
                            <Col md={6}>
                                    <Form.Group className="mb-3" controlId="pcform.zip_code">
                                        <Form.Label>Zip Code  <strong style={{color:"red"}}>*</strong></Form.Label>

                                        <input className="form-control" type="text"
                                            {...register("zip_code", { required: true })}
                                        />
                                        {/* {errors.zip_code && errors.zip_code.type === "required" &&
                                            <p style={{ color: "red" }}>Zip Code is required</p>} */}
                                    </Form.Group>
                                </Col>
                            </Row>
                    
                         
                            <Col md={6} >
                                <Form.Group className="mb-3" controlId="pcform.phone">
                                    <Form.Label> Phone Number <strong style={{color:"red"}}>*</strong></Form.Label>

                                    <input
                                        type="text"
                                        className={`form-control ${errors.phone && "invalid"}`}
                                        {...register("phone", {
                                            required: "Phone is Required",
                                            pattern: {
                                                value: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                                                message: "Invalid phone no",
                                            },
                                        })}
                                        onKeyUp={() => {
                                            trigger("phone");
                                        }}
                                    />
                                   {errors.phone && (
                                            < p style={{ color: "red" }}>{errors.phone.message}</p>
                                        )} 
                                </Form.Group>
                            </Col>

                            <Form.Group className="mb-3" controlId="pcform.submit">
                                <input type="submit" className="form-control btn btn-primary"
                                    style={{ width: "20%", float: "right", marginTop: "2%", 
                                    backgroundColor: "#687080", borderBlockColor: "#687080", 
                                    fontWeight: "bold", borderRadius: "20px", marginRight: "5%" }} />
                            </Form.Group>
                        </form>
                        <strong> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
                        <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong>

                    </Card.Body>


                </Card>
                <div className="w-100 text-center mt-3">
                    <strong>  Already have an account? <Link to='/userlogin'>Log In</Link></strong>
                </div>
                {/* </Container> */}
            </div>
        </div>

    )
}

export default Signup


