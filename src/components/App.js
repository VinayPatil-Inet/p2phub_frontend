import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
// import Login from './Login';
import Signup from './Signup';
import { Container } from 'react-bootstrap';
import FormFill from './FormFill';
import NewPayer from './NewPayer';
import TicketRequest from './TicketRequest';
import ViewPayer from './ViewPayer';
import Billing from './Billing';
import Contract from './Contract';
import MyPayerList from './MyPayerList'
import PayersRegistered from './PayersRegistered';
import Reports from './Reports';
import ManagerDashboard from './ManagerDashboard';
import PayerContactList from '../components/PayerContactForm/PayerContactList';
import SearchPayer from './SearchPayer';
import AddPayerContact from '../components/PayerContactForm/AddPayerContact'
import Userlogin from './UserLogin'
import NewOrganization from './NewOrganization';
import ViewProfile from './ViewProfile';
import BillingDetails from './BilingDetails';
import Navbar from './NavBar';
import Documentation from './Documentation';
import UpdatePayerContact from './PayerContactForm/UpdatePayerContact';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import AdminUser from './AdminUser';
import AdminDashboard from './AdminDashboard'



function App() {
  return (
    <Container className='d-flex align-items-center justify-content-center' style={{ minHeight: '100vh' }}>
      <div className='w-100'>

        

        <Router>

          <Switch>
            {/* <Route exact path='/userlogin' component={Userlogin} /> */}
            <Route exact path='/' component={Userlogin} />
            <Route exact path='/signup' component={Signup} />
             <Route exact path='/userlogin' component={Userlogin} /> 
            <div>
              
              <Navbar />

              <Route  path="/ticketRequest" component={TicketRequest}></Route>
              <Route  path="/Billing" component={Billing}></Route>
              <Route path='/form' component={FormFill} />
              <Route path='/MyPayerList/:id' component={MyPayerList} />
             <Route path='/updatePayer/:id' component={UpdatePayerContact} /> 
              <Route path='/newPayer' component={NewPayer} />
              <Route path='/viewPayer/:value' component={ViewPayer} />
              <Route path='/BillingDetails/:id' component={BillingDetails} />
              <Route path='/Contract' component={Contract} />
              <Route path='/PayersRegistered/:id' component={PayersRegistered} />
              <Route path='/reports' component={Reports} />
              <Route path='/managerDashboard' component={ManagerDashboard} />
              <Route path='/payerContactList' component={PayerContactList} />
              <Route path='/searchPayer' component={SearchPayer} />
              <Route path='/editPayerContact' component={UpdatePayerContact} />
              <Route path='/addPayerContact' component={AddPayerContact} />
              <Route path='/newOrganization' component={NewOrganization} />
              <Route path='/viewProfile' component={ViewProfile} />
              <Route path='/documentation' component={Documentation} />
              <Route path='/ForgotPassword' component={ForgotPassword} />
              <Route path='/ResetPassword' component={ResetPassword} />
              <Route path='/CreateAdminUser' component={AdminUser} />
              <Route path ='/AdminDashboard' component={AdminDashboard}/>
            
             

            </div>

          </Switch>
        </Router>
        {/* 
        <Router>
        <Route exact path='/userlogin' component={Userlogin} />
          <Navbar />
          <Switch>
           /* <Route path='/MyPayerList' component={MyPayerList} /> *
            <Route path='/ticketRequest' component={TicketRequest} />
            <Route path='/Billing' component={Billing} />
            <Route path='/form' component={FormFill} />
            <Route path='/MyPayerList/:id' component={MyPayerList} />
            <Route path='/MyPayerList' component={MyPayerList} />
            <Route path='/newPayer' component={NewPayer} />
            <Route path='/viewPayer/:id' component={ViewPayer} />
            <Route path='/BillingDetails/:id' component={BillingDetails} />
            <Route path='/Contract' component={Contract} />
            <Route path='/PayersRegistered' component={PayersRegistered} />
            <Route path='/reports' component={Reports} />
            <Route path='/managerDashboard' component={ManagerDashboard} />
            <Route path='/payerContactList' component={PayerContactList} />
            <Route path='/searchPayer' component={SearchPayer} />
            <Route path='/editPayerContact' component={UpdatePayerContact} />
            <Route path='/addPayerContact' component={AddPayerContact} />
            <Route path='/newOrganization' component={NewOrganization} />
            <Route path='/signup' component={Signup} />
         
          
          </Switch>
          
        </Router> */}

        {/* <Router>
          <AuthProvider>
            <Switch>
              <PrivateRoute exact path='/' component={Dashboard} />
              <Route path='/signup' component={Signup} />
              <Route path='/Home' component={Navbar} />
             
              <Route path='/userlogin' component={Userlogin} />
              <Route path='/form' component={FormFill} />
              <Route path='/MyPayerList/:id' component={MyPayerList} />
              <Route path='/MyPayerList' component={MyPayerList} />
              <Route path='/newPayer' component={NewPayer} />
              <Route path='/viewPayer/:id' component={ViewPayer} />
           
              <Route path='/ticketRequest' component={TicketRequest} />
              <Route path='/Billing' component={Billing} />
              <Route path='/BillingDetails/:id' component={BillingDetails} />
              <Route path='/Contract' component={Contract} />
              <Route path='/PayersRegistered' component={PayersRegistered} />
              <Route path='/reports' component={Reports} />
              <Route path='/managerDashboard' component={ManagerDashboard} />
              <Route path='/payerContactList' component={PayerContactList} />
              <Route path='/searchPayer' component={SearchPayer} />
              <Route path='/editPayerContact' component={UpdatePayerContact} />
              <Route path='/addPayerContact' component={AddPayerContact} />
              <Route path='/newOrganization' component={NewOrganization} />
             
            </Switch>
          </AuthProvider>
        </Router> */}
      </div>
    </Container>
  );
}
{/* <Route path='/viewPayer' component={ViewPayer} /> */ }
{/* <Route path='/login' component={Login} /> */ }
export default App;