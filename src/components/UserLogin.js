import React, { useEffect, useRef, useState } from 'react';
import { Form, Button, Card, Alert } from 'react-bootstrap';
import { useAuth } from '../contexts/AuthContext';
import { Link, useHistory } from 'react-router-dom';
import { async } from '@firebase/util';
import { Redirect } from "react-router-dom";
import Img from './healthchain-logo.png'
import { set, useForm } from "react-hook-form";
import axios from 'axios';



const UserLogin = () => {
  const INITIAL = {
    email: "",
    password: ""

  }
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
      const succmessage = urlParams.get('success');
      console.log("Login useEffect",succmessage);
      setTimeout(() => {
        setSuccessMessage()
      }, 2000);
      setSuccessMessage(succmessage);
  }, []);

  var url = "http://localhost:7000/api/user/login"

  const [data, setData] = useState(INITIAL);
  const [message, setMessage] = useState(null);

  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = React.useState("");

  const { register, reset, formState: { errors } } = useForm();


  function handleChange(e) {
    const { id, value } = e.target; //destructuring 
    setData({ ...data, [id]: value })
  }
 

  function handleSubmit(e) {
    e.preventDefault();
  
    axios.post(url, data)
      .then(res => {
       
        if (res.data.code === 200) {
         
          sessionStorage.setItem('userid', res.data.results[0].user_id);
          sessionStorage.setItem('payerid',res.data.results[0].payer_id)
          sessionStorage.setItem('email',res.data.results[0].email)
          sessionStorage.setItem('isadmin',res.data.results[0].is_admin)
          sessionStorage.setItem('ishcadmin',res.data.results[0].is_hc_admin)
          sessionStorage.setItem('name',res.data.results[0].first_name+' '+res.data.results[0].last_name)
          if(res.data.results[0].orgemail != null){
            history.push('/searchPayer');
          }
          else{
            history.push('/newOrganization');
          }
         
        }
      
        console.log(res.data, "handleSubmit On Login")
        console.log(res.data.results[0].user_id, " --- result")
      })
      .catch(error => { 

         if (error.data != 200) {
          setErrorMessage("Email and password does not match")
        reset(error.data);
        setTimeout(() => {
            setErrorMessage()
          }, 2000);
        }
        console.log(error.data,"padma");
      })
    
     
  }

  return (

    <div style={{ width: '40%', marginLeft: '30%' }}>
      <Card>
        <Card.Body>
          <center>
        <img src= {Img} alt="pic" width={150}/></center>
        <hr></hr>
        <div><h4 className='text-center mb-4 '><strong> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong></h4></div>
          <h2 className='text-center mb-4 '>Log In</h2>
          <form onSubmit={handleSubmit} >

            <Form.Group className="mb-3" controlId="pcform.email">
              <Form.Label>Email</Form.Label>
              <input type="text" name="email" id="email" type='email'
                onChange={handleChange}
                value={data.email} className="form-control"

              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="pcform.password">
              <Form.Label>Password</Form.Label>
              <input className="form-control"
                name="password" id="password" type='password'
                onChange={handleChange}
                value={data.password}
              />
            </Form.Group>
            <div className="w-100 text-center mt-3">
                    <strong style={{ marginLeft: "70%" }}> <Link to='/ForgotPassword'>Forgot Password</Link></strong>
                </div>
            <center><Button type='submit'  disabled={!data.email || !data.password}
            className='w-90 mt-4'>Log In</Button></center>

    
          </form>
          <strong> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
          <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong>

        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-3">
        <strong>Want to register with us? <Link to='/signup'>Sign Up</Link></strong>
      </div>
    </div>

  )



  // const emailRef = useRef();
  // const passwordRef = useRef();
  // const { login } = useAuth();
  // const [error, setError] = useState('');
  // const [loading, setLoading] = useState(false);
  // const history = useHistory();

  // async function handleSubmit(e) {
  //     e.preventDefault();
  //     try {
  //         setError('');
  //         setLoading(true);
  //         await login(url,emailRef.current.value, passwordRef.current.value);
  //         history.push('/');
  //     } catch (err) {
  //         setError('Filed to log in to your account');
  //     }
  //     setLoading(false);
  // }

  // return (
  // <div style={{ width:'40%', marginLeft:'30%' }}>
  //     <Card>
  //         <Card.Body>
  //             <h2 className='text-center mb-4'>Log In</h2>
  //             {error && <Alert variant='danger'>{error}</Alert>}
  //             <Form onSubmit={handleSubmit}>
  //                 <Form.Group id='email'>
  //                     <Form.Label>Email</Form.Label>
  //                     <Form.Control type='email' ref={emailRef} required></Form.Control>
  //                 </Form.Group>
  //                 <Form.Group className='mt-4' id='password'>
  //                     <Form.Label>Password</Form.Label>
  //                     <Form.Control type='password' ref={passwordRef} required></Form.Control>
  //                 </Form.Group>
  //                 <Button type='submit' disabled={loading} className='w-100 mt-4'>Log In</Button>
  //             </Form>
  //         </Card.Body>
  //     </Card>
  //     <div className="w-100 text-center mt-2">
  //         Want to register with us? <Link to='/signup'>Sign Up</Link>
  //     </div>
  // </div>
  // )
}

export default UserLogin;








