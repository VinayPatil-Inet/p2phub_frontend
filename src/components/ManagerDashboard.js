import { Container, Row, Col, Form, Button, Card, InputGroup } from "react-bootstrap";
import { Link, useHistory, useParams } from 'react-router-dom';
import React, { Component, useState, useEffect } from 'react';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory from 'react-bootstrap-table2-filter'
import BootstrapTable from "react-bootstrap-table-next";
import "bootstrap/dist/css/bootstrap.css";
import _ from 'lodash'
import axios from 'axios';

let ManagerDashboard = () => {

   // let user_id = useParams();
    const [userList, setUserList] = useState([]);
    useEffect(() => {
        getPayerList()
    }, []);

    function getPayerList() {
        axios.get('http://localhost:7000/api/users/get/AdminUsers').then(res => {
            setUserList(res.data.data);
            console.log(res.data, "padma");
        });
    }
    const columns = [
        //  { dataField: 'user_id', text: 'Id' },
  
        { dataField: 'username', text: 'Name' },
        { dataField: 'email', text: 'Email' },
        { dataField: 'phone', text: 'Phone' },
        // { name: '#',selector: 'serial'
        // },

        {
            dataField: "remove",
            text: "View",
            formatter: (cellContent, row) => {
                return (
                    <Link to={`/PayersRegistered/${row.user_id}`}>
                        <button
                            className="btn btn-success btn-xs" Width="95px"
                            style={{ margin: "2px" }}
                            onClick={() => getIdPayerList(row.user_id)}
                        >
                            View
                        </button>

                    </Link>

                );
            },
        },
        
    ]
    // userList.forEach((user, index) => {
    //     user.serial = index + 1;
    //  })
    function getIdPayerList(id) {
        fetch(`http://localhost:7000/api/user/getIdByUser/${id}`, {
            method: 'get'

        }).then((result) => {
            result.json().then((resp) => {
                console.log(resp, "padma")
                console.log(resp.data, "padma")
            })
        })
            .catch(error => {
                console.log(error.data)
            });

    }


    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 5,
        lastPageText: '>>',
        firstPageText: '<<',
        nextPageText: '>',
        prePageText: '<',
        showTotal: true,
        alwaysShowAllBtns: true,
        onPageChange: function (page, sizePerPage) {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        },
        onSizePerPageChange: function (page, sizePerPage) {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        }
    })

    return (
        <Container fluid="md">
            <Row>
                <Col>
                    <Card style={{ width: "100%", align: "center" }}>
                        <h3 className='text-center mb-3 heading'>Manager Dashboard</h3>
                        <center><hr className='heading_line'></hr></center>
                        <Card.Body>

                            <div className="container-fluid p-3">
                                <BootstrapTable bootstrap4 keyField='id'
                                    columns={columns}
                                    data={userList}
                                    pagination={pagination}
                                    filter={filterFactory()}
                                />
                            </div>
                        </Card.Body>
                    </Card>

                </Col>

            </Row>

            {/* < strong > {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
            <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong> */}


        </Container >
    );

}

export default ManagerDashboard;



// 