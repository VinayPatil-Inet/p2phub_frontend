import React, { Component,useState, useEffect  } from "react";
import { NavLink } from "react-router-dom";
// gsap
import { TweenLite, Power1,TimelineLite } from "gsap";
// styles
import "./react-sidenav.css";

// menu data, could be imported from another file or fetched from a server
const menuItems = [
  // { type: "link", url: "/", title: "login", icon: "fa-th-large" },
  { type: "link", url: "/newPayer", title: "New Payer", icon: "fa-user" },
   { type: "link", url: "/CreateAdminUser", title: "Create HC User",icon: "fa-user"},
 // { type: "link", url: "/ticketRequest", title: "Ticket Request",icon: "fa-user"},
//  { type: "link", url: "/Contract", title: "Contract",icon: "fa-user" },
  { type: "link", url: "/managerDashboard", title: "Manager Dashboard",icon: "fa-th-large" },
  { type: "link", url: "/documentation", title: "Documentation",icon: "fa-user" },
  { type: "link", url: "/payerContactList", title: "Payer Contact List",icon: "fa-bars" },
 // { type: "link", url: "/addPayerContact", title: "Add Payer Contact",icon: "fa-user" },
  { type: "link", url: "/searchPayer", title: "DashBoard",icon: "fa-search"},
 // { type: "link", url: "/PayersRegistered", title: "PayersRegistered",icon: "fa-list" },
 // { type: "link", url: "/Documentation", title: "Documentation",icon: "fa-user" },
  { type: "link", url: "/reports", title: "Reports" ,icon: "fa-flag"},
 // { type: "link", url: "/Billing", title: "Billing",icon: "fa-user" },
  { type: "link", url: "/newOrganization", title: "Organization Profile",icon: "fa-user"  },
 // { type: "link", url: "/viewProfile", title: "Organization Profile",icon: "fa-user"  },
  { type: "link", url: "/userlogin", title: "Logout",icon: "fa-sign-out-alt" },
  { type: "link", url: "/viewProfile", title: "View Profile",icon: "fa-user" },

 

];


class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      menuOpen: false
    };
    this.getusers();
    this.drawer = null;
    this.menuBtn = null;
    this.contentVeil = null;
    
    this.drawerTween = new TimelineLite({
      paused: true
    });
    
    this.toggleContentVeil = this.toggleContentVeil.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.veilClickHandler = this.veilClickHandler.bind(this);
  }

  toggleContentVeil() {
    TweenLite.set( this.contentVeil, {
      autoAlpha: this.drawerTween.reversed() ? 0 : 0.25
    });
  }

  toggleDrawer() {
    this.drawerTween.reversed( !this.drawerTween.reversed() );
    this.setState({
      menuOpen: !this.state.menuOpen
    });
  }

  veilClickHandler(e) {
    e.stopPropagation();
    this.toggleDrawer();
  }

  componentDidMount() {
    this.drawerTween
      .call(this.toggleContentVeil)
      .to( this.drawer, 0.25, {
        x: 0, ease: Power1.easeOut
      })
      .to( this.menuBtn, 0.25, {
        x: 170, ease: Power1.easeOut
      }, 0)
      .reverse();
  }
  
  getusers()
  {
    console.log("getusers called");
      var email = sessionStorage.getItem('email');
        var name = sessionStorage.getItem('name');
        console.log("getusers called",name,email);
  }

  render() {
   
    return <nav className="drawer-wrap">
     {/* <div className="container-fluid menu-header"> 
       
          <div className="row">
            <div className="col-12">
              <div className="h2 text-center ">SideBar Slide Menu</div>
            </div>
          </div>
      </div>   */}

      <div className="drawer"
        ref={ e => this.drawer = e }
      >
        <div className="drawer-header">
          <h5 className="pl-3"><p>{sessionStorage.getItem('email')}</p>{sessionStorage.getItem('name')}</h5>
        </div>


     
        <div className="menu-container py-3">
          {
            menuItems.map( e => {
              if( e.type == "divider" ) {
                return <div className="divider" key={e.title}></div>;
              } else {
                return <NavLink key={e.title}
                  exact
                  to={e.url}
                  className="menu-item"
                  onClick={this.toggleDrawer}
                >
                 <i className={`fas ${e.icon} mr-3`}></i>{e.title} 
                </NavLink>;
              }
            })
          }
        </div>

      </div>

      <button className="btn shadow-sml menu-button"
        ref={ e => this.menuBtn = e }
        onClick={ this.toggleDrawer }
      >
        <i className={`fas ${this.state.menuOpen ? "fa-times" : "fa-bars"}`}></i>
      </button>

      <div className="content-veil"
        ref={ e => this.contentVeil = e }
        onClick={this.veilClickHandler}
      ></div>

    </nav>;
  }

}

export default Home;
