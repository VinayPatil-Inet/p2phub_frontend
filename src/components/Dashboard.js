import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";

const Dashboard = () => {
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    setError("");

    try {
      console.log("at logout");
      await logout();
      history.push("/");
    } catch (error) {
      setError("Failed to Logout");
    }
  }

  return (
    <>
      <Card>
        <Card.Body>
          <h2 className="text-center mb-4">Dashboard</h2>
          <strong>Email:</strong> {currentUser.email}
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <Button variant="link" onClick={handleLogout}>
          Logout
        </Button>
        <br />
        <Link to="/form">Profile</Link>
        <br />
        {/* <Link to="/viewPayer/:id">View Payers</Link>
        <br /> */}
        <Link to="/NewPayer">New Payer</Link>
        <br />
        <Link to="/MyPayerList">My Payer List</Link>
        <br />
        <Link to="/addPayerContact"> Add Payer Contact Form</Link>
        <br />
        <Link to="/PayerContactList">Payer Contact List</Link>
        <br />
        <Link to="/Contract">Contract</Link>
        <br />
        <Link to="/TicketRequest">Ticket Request</Link>
        <br />

        <Link to="/PayersRegistered">Payers Registered</Link>
        <br />
        <Link to="/Billing">Billing</Link>
        <br />
        <Link to="/Reports">Reports</Link>
        <br />
        <Link to="/managerDashboard">Manager Dashboard</Link>
        <br />
        <Link to="/searchPayer">Search Payer</Link>
        <br/>
        <Link to="/newOrganization">New Profile</Link>
        
      </div>
      
    </>
  );
};

export default Dashboard;
