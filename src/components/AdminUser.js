

import React, { Component, useState, useMemo, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { Form, Button, Container, custom, Card, Row, Col, Alert } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';
import { MDBInput } from "mdbreact";
import './css/custom.css'; //Import here your file style


const AdminUser = () => {

    //form validation rules 
    const validationSchema = Yup.object().shape({

        password: Yup.string()
            .required('Password is required'),
        confirm_password: Yup.string()
            .required('Confirm Password is required')
            .oneOf([Yup.ref('password')], 'Passwords must match')

    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, trigger, formState } = useForm();
    const { errors } = formState;
    const [errorMessage, setErrorMessage] = React.useState("");
    const [successMessage, setSuccessMessage] = React.useState("");
    const [usertypeList, setUserTypeList] = useState([]);

    const onSubmit = data => {
     //   console.log(data, "data")
        axios

            .post(
                'http://localhost:7000/api/user/create/AdminUser',
                data,
            )
            .then(response => {
                setSuccessMessage("Successfully created a  admin user!")
                reset(response.data);
                setTimeout(() => {
                    setSuccessMessage()
                }, 2000);
                console.log(response.data, "ppppppppppp")
            })
            .catch(error => {
                setErrorMessage("Cannot created  user")
                reset(error.data);
                setTimeout(() => {
                    setErrorMessage()
                }, 2000);
                console.log(error.data, "pppppppppp")
            });
    };
 

    return (
        <div class="signup_header">
            <div style={{ width: '40%', marginLeft: '30%' }}>
                <Card>
                    <Card.Body>
                     
                        <h3 className='text-center mb-4 heading2'>Register</h3>
                        <form onSubmit={handleSubmit(onSubmit)}>
                        <Form.Group className="mb-3" controlId="pcform.password">
                                        <Form.Label>Username <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("username", { required: true})}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>  
                                   
                                    <Form.Group className="mb-3" controlId="pcform.email">
                                        <Form.Label>Email  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("email", { required: true, pattern: /^\S+@\S+$/i })}
                                        />
                                     {/* {errors.email && errors.email.type === "required" &&
                                             <h4 style={{color:"red",marginRight:"90%"}}>*</h4>} */}
                                        {errors.email && errors.email.type === "pattern" && <p style={{ color: "red" }}>Invalid Email</p>} 
                                    </Form.Group>
                             
                                    <Form.Group className="mb-3" controlId="pcform.password">
                                        <Form.Label>Password  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="password"
                                            {...register("password", { required: true, maxLength: 60 })}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>
                                <Form.Group className="mb-3" controlId="pcform.phone">
                                    <Form.Label> Phone  <strong style={{color:"red"}}>*</strong></Form.Label>

                                    <input
                                        type="text"
                                        className={`form-control ${errors.phone && "invalid"}`}
                                        {...register("phone", {
                                           // required: "Phone is Required",
                                            pattern: {
                                                value: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                                                message: "Min number 10 ",
                                            },
                                        })}
                                        onKeyUp={() => {
                                            trigger("phone");
                                        }}
                                    />
                                   {errors.phone && (
                                            < p style={{ color: "red" }}>{errors.phone.message}</p>
                                        )} 
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="pcform.address1">
                                        <Form.Label>Address  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("address1", { required: true})}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="pcform.city">
                                        <Form.Label>City  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("city", { required: true })}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="pcform.state">
                                        <Form.Label>State  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control" type="text"
                                            {...register("state", { required: true})}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="pcform.zip_code">
                                        <Form.Label>Zip Code  <strong style={{color:"red"}}>*</strong></Form.Label>
                                        <input className="form-control"type="text"
                                            {...register("zip_code", { required: true })}
                                        />
                                        {/* {errors.password && errors.password.type === "required" && <p style={{ color: "red" }}>Password is required</p>} */}
                                    </Form.Group>  
                                   
                           

                            <Form.Group className="mb-3" controlId="pcform.submit">
                                <input type="submit" className="form-control btn btn-primary"
                                    style={{ width: "20%", float: "right", marginTop: "2%", 
                                    backgroundColor: "#687080", borderBlockColor: "#687080", 
                                    fontWeight: "bold", borderRadius: "20px", marginRight: "5%" }} />
                            </Form.Group>
                        </form>
                        <strong> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
                        <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong>

                    </Card.Body>


                </Card>
                <div className="w-100 text-center mt-3">
                    <strong>  Already have an account? <Link to='/userlogin'>Log In</Link></strong>
                </div>
                {/* </Container> */}
            </div>
        </div>

    )
}

export default AdminUser


