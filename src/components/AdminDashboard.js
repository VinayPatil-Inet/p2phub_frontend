import { Container, Row, Col, Form, Button, Card, InputGroup } from "react-bootstrap";
import { Link, useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faSearch, faPlus, faEye, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import React, { Component, useState, useEffect } from 'react';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import BootstrapTable from "react-bootstrap-table-next";
import "bootstrap/dist/css/bootstrap.css";
import _ from 'lodash'
import axios from 'axios';




let AdminDashboard = () => {





    const [userList, setUserList] = useState([]);

    const [payerList, setPayerList] = useState([]);
    const [errorMessage, setErrorMessage] = React.useState("");
    const [successMessage, setSuccessMessage] = React.useState("");

    useEffect(() => {
        getPayerList()
      //  getRequestePayerList()

    }, []);
    const columns = [
        { dataField: 'payer_id', text: 'Id' },
        { dataField: 'name', text: 'Payer Name'}, //filter: textFilter() },
        { dataField: 'email', text: 'Payer Email'}, //filter: textFilter() },
        { dataField: 'payer_url', text: 'Payer Url' },


        /*{
            dataField: "remove",
            text: "Add to My List",
            formatter: (cellContent, row) => {
                return (
                    <Link to={`/MyPayerList/${row.id}`}>
                        <button
                            className="btn btn-primary btn-xs" Width="95px"
                            style={{ margin: "2px" }}
                            onClick={() => addMyList(row.id)}
                        >
                            Add MyList
                        </button>

                    </Link>

                );
            },
        },*/
        {
            dataField: "remove",
            text: "View",
            formatter: (cellContent, row) => {
                return (
                    <Link to={`/viewpayer/${row.payer_id}`}>
                        <button
                            className="btn btn-success btn-xs" Width="95px"
                            style={{ margin: "2px" }}
                            onClick={() => getIdPayerList(row.payer_id)}
                        >
                            View
                        </button>

                    </Link>

                );
            },
        },
    ]
    function getIdPayerList(payer_id) {
        fetch(`http://localhost:7000/api/getOrganisationId/${payer_id}`, {
            method: 'get'

        }).then((result) => {
            result.json().then((resp) => {

                console.log(resp, "padma")
                console.log(resp.data.id, "padma")
            })
        })
            .catch(error => {

                console.log(error.data)
            });

    }

    function addMyList(id) {
        fetch(`http://localhost:7000/api/addMylist/${id}`, {
            method: 'get'

        }).then((result) => {
            result.json().then((resp) => {

                console.log(resp, "padma")
                console.log(resp.data.id, "padma")
            })
        })
            .catch(error => {

                console.log(error.data)
            });

    }
    function contract(id) {
        fetch(`http://localhost:7000/api/getIdContractContentsdDetails/${id}`, {
            method: 'get'

        }).then((result) => {
            result.json().then((resp) => {

                console.log(resp, "padma")
                console.log(resp.id, "padma")
            })
        })
            .catch(error => {

                console.log(error.data)
            });

    }

    // function getRequestePayerList() {
    //     axios.get('http://localhost:7000/api/getAllPayers').then(res => {
    //         setPayerList(res.data.data);
    //         console.log(res.data.data, "sucess1");
    //     });
    // }

    function getPayerList() {
        axios.get('http://localhost:7000/api/getAllOrganisation').then(res => {
            setPayerList(res.data.data);
            console.log(res.data.data, "sucess");


        });
    }
 //   getRequestePayerList()
    const columnsList = [
        { dataField: 'name', text: 'Search By Name' },
        { dataField: 'email', text: 'Search By Email' },
        { dataField: 'payer_url', text: 'Payer Url' },
        // {dataField:'status',text:'Status'},


        {
            dataField: "remove",
            text: "Status",
            formatter: (cellContent, row, payerList) => {
                if (row.status === 'Unsigned') {
                    return (
                        <Link to={`/Contract/${row.id}`}>
                            <button
                                className="btn btn-danger btn-xs" Width="95px"
                                style={{ margin: "2px" }}
                                onClick={() => contract(row.id)}
                            >
                                Unsigned
                            </button>

                        </Link>

                    );
                }
                else if (row.status === 'Signed') {
                    return (
                        <button
                            className="btn btn-success btn-xs" Width="95px"
                            style={{ margin: "2px" }}
                        >
                            Signed
                        </button>
                    );
                }
            },
        },
        // {
        //     dataField: "remove",
        //     text: "Action",
        //     formatter: (cellContent, row) => {
        //         return (
        //                 <button
        //                     className="btn btn-warning btn-xs" Width="95px"
        //                     style={{margin:"2px"}}
        //                 >
        //                   Add App 
        //                 </button>
        //         );
        //     },
        // },

    ]
    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 5,
        lastPageText: '>>',
        firstPageText: '<<',
        nextPageText: '>',
        prePageText: '<',
        showTotal: true,
        alwaysShowAllBtns: true,
        onPageChange: function (page, sizePerPage) {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        },
        onSizePerPageChange: function (page, sizePerPage) {
            console.log('page', page)
            console.log('sizePerPage', sizePerPage)
        }
    })




    const getColor = (status) => {
        if (status === 'Unsigned') return 'red';
        if (status === 'Signed') return 'green';
        return '';
    };
    return (
        <Container fluid="md">


            <Row>
                 <Col md={1} style={{ textAlign: "left" }}>
                    {/* <Card border="primary" style={{ width: "100%", align: "center", marginRight: "100%" }}>
                <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>My  Contact Payer List</Card.Header>

              

            </Card > */}
                    
                       
                       

                            
                            {/* <table class="table table-sm mt-3">
        
        <thead class="thead-dark">
            <th>Payer Name</th>
            <th>Payer Email</th>
            <th>Payer URL</th>
            <th>Status </th>
            <th> </th>
           <th>Action</th>
           <th> </th>
        </thead>

        <tbody>
            { payerList.map(x =>
             <tr >
                <td>{x.name}</td>
                <td>{x.email}</td>
                <td>{x.payer_url}</td> */}
                            {/* <td style={{ color: getColor(x.status) }}>{x.status}</td> 
               */}

                            {/*               
             <td >
             <Button  variant="warning" style={{ float: 'right' , }} size="sm"> App Deatils</Button>{' '}
                </td>   
           <td>  <Link to={`/Contract`}>
               <Button  variant="danger" style={{ float: 'right' }}   size="sm">Unsigned</Button>
               </Link> 
               
                </td>
              
           
            <td> 
                
                <Button variant="success" style={{ float: 'right' }} size="sm">Signed</Button>{' '}
             </td>
        
            </tr>)}


            
            {userList.length == 0 && <tr>
                <td className="text-center" colSpan="4">
                    <b>No data found to display.</b>
                </td>
            </tr>}
        </tbody>
    </table>   */}


                       
                    

                </Col>
                <Col md={8} style={{ textAlign: "left" }}>
                    <Card style={{ width: "120%", align: "center" }} >
                      {/*  <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff", }}> Payers List </Card.Header> */}
                      <h3 className='text-center mb-3 heading'> Payer List </h3>
                        <center><hr className='heading_line'></hr></center>
                        <br></br>
                        

                        <div className="w-100 text-center mt-2">
                            <Link to='/newOrganization'><Button variant="warning" style={{ marginLeft: "70%" }}> <FontAwesomeIcon icon={faPlusCircle}  > </FontAwesomeIcon> Add New Organization</Button> </Link>
                        </div>
                        <div className="container-fluid p-3">
                            <BootstrapTable hover keyField='id'
                                columns={columns}
                                data={payerList}
                                pagination={pagination}
                                filter={filterFactory()}
                            />


                        </div>


                    </Card >

                </Col>

            </Row>

            < strong > {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
            <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong>


        </Container >
    );

}

export default AdminDashboard;



// const SearchPayer = () => {

//  const [userList, setUserList] = useState([]);
//     const [search, setSearch] = useState("");

//     useEffect(() => {
//         getPayerList();


//     }, []);

//     function getPayerList() {
//         axios.get('http://localhost:7000/api/getAllrequestedpayers').then(res => {
//             setUserList(res.data.data);
//             console.log(res.data, "padma")
//         });
//     }
//     function getIdPayerList(id) {
//                 fetch(`http://localhost:7000/api/getIdrequestedpayers/${id}`, {
//                     method: 'get'

//                 }).then((result) => {
//                     result.json().then((resp) => {

//                         console.log(resp.data,"ppppppppppp")


//                     })
//                 })
//                 .catch(error => { 

//                     console.log(error.data) });

//             }


//     return (

//         <Container fluid="md">
//             <Card border="primary">
//                 <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }} >
//                     <div style={{ width: "100%", height: "35px" }}>
//                         Search Payer
//                     </div>
//                     <div className="w-100 text-center mt-2">
//                         <Link to='/newPayer'><Button variant="warning" style={{ marginLeft: "80%" }}> <FontAwesomeIcon icon={faPlusCircle}  > </FontAwesomeIcon> Add Payer</Button> </Link>
//                     </div>
//                 </Card.Header>
//                 <Card.Body>
//                     <Row>
//                         <Col md={6} style={{ width: "40%", marginLeft: "30%" }}>
//                             <Form.Row>
//                                 <Form.Group as={Col}>
//                                     <InputGroup>
//                                         <Form.Control type="text" placeholder="Search By"
//                                             onChange={(e) => {
//                                                 setSearch(e.target.value);
//                                             }} />
//                                         <InputGroup.Prepend >
//                                             <InputGroup.Text >
//                                                 <FontAwesomeIcon icon={faSearch} className="mb-2" />
//                                             </InputGroup.Text>
//                                         </InputGroup.Prepend>
//                                     </InputGroup>
//                                 </Form.Group>

//                             </Form.Row>

//                         </Col>
//                         {userList.length == 0 &&
//                             <td className="text-center" colSpan="4">
//                                 <b>No data found to display.</b>
//                             </td>
//                         }
//                     </Row>

//                     {userList.filter((val) => {
//                         if (search === "") {
//                             return val
//                         }
//                         else if (
//                             val.email.toLowerCase().includes(search.toLocaleLowerCase()) ||
//                             val.name.toLowerCase().includes(search.toLocaleLowerCase())

//                         ) {
//                             return val

//                         }

//                     })
//                         .map(x => (
//                             <Row>
//                                 <Col md={8}>
//                                     <p style={{ color: "#096DFF", margin: "1px", fontWeight: "bold" }}> {x.name}</p>
//                                     <p style={{ margin: "1px" }}><b>Email</b> - {x.email}</p>
//                                     <p style={{ margin: "1px" }}><b>Payer URL</b>- {x.payer_url}</p>

//                                 </Col>
//                                 <Col md={4} style={{ textAlign: "right" }}>
//                                     <div className="w-100 text-center mt-2">
//                                     </div>
//                                     <Button variant="primary" style={{ margin: "2px", marginTop: "20px" }}>
//                                         <FontAwesomeIcon icon={["fas", "coffee"]} /><FontAwesomeIcon icon={faPlusCircle} > </FontAwesomeIcon> Add to My List</Button>

//                                     <Link to={`/viewpayer/${x.id}`}>
//                                         <Button variant="success"  onClick={() => getIdPayerList(x.id)} style={{ margin: "2px", marginTop: "20px" }}>
//                                             <FontAwesomeIcon icon={faEye} ></FontAwesomeIcon> View</Button>
//                                     </Link>


//                                 </Col>
//                                 <Col md={12}>
//                                     <hr />
//                                 </Col>
//                             </Row>
//                         ))}
//                 </Card.Body>
//             </Card>
//         </Container>


//     )
// }

// export default SearchPayer;

