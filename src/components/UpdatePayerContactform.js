import React, { Component, useState, useEffect } from 'react';
import './css/Form.css'; //Import here your file style
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import axios from 'axios';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

const btn = {
    "width": "20%",
    "textAlign": "center",
    "marginLeft": "40%",
}

const col = {
    "margin": "6px"
};

const sheader = {
    "backgroundColor": "blue",
    "color": "#ffffff"
}

const sheader_inner1 = {
    "backgroundColor": "gray",
    "color": "white"
}


let PayerContactForm = () => {

    const baseURL = "http://localhost:7000/api/GetAllPayercontact";

 
      const [post, setPost] = React.useState(null);
    
      React.useEffect(() => {
        axios.get(`${baseURL}`).then((response) => {
          setPost(response.data);
          console.log(response.data)
        });
      }, []);
    
      function updatePost() {
        axios
          .put(`${baseURL}/80`, {
       
            name:'',
            email:'',
            subject:'', message:'', inserted_by:''
          })
          .then((response) => {
            setPost(response.data);
            console.log(response.data,"padma")
          });
         
      }
    
      if (!post) return "No post!"
    
      return (
        <div>
            
          <h1>{post.name}</h1>
          <p>{post.email}</p>
          <p>{post.subject}</p>
          <p>{post.message}</p>
         
          <p>{post.inserted_by}</p>
          <button onClick={updatePost}>Update Post</button>
        </div>
      );
    }





export default PayerContactForm;
