// import './css/Form.css';
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import React, { Component, useState, useEffect } from 'react';
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faSearch, faPlus, faEye, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { Link, useHistory, useParams } from 'react-router-dom';

import { CircularProgress } from "@material-ui/core";
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import BootstrapTable from 'react-bootstrap-table-next';

const ViewPayer =() =>{
  const { value } = useParams();
  const [userList, setUserList] = useState([]);
  const columns = [
    { dataField: 'payer_id', text: 'Id' },
   { dataField: 'name', text: 'Name' },
   { dataField: 'email', text: 'Email' },
   { dataField: 'payer_url', text: 'Payer Url' },
  // { dataField: 'is_approve', text: 'Action' },
   {
    dataField: "is_approve",
    text: "Action",
    formatter: (cellContent, row) => {
        return (
          <div className="w-100 text-center mt-2">
                       
          <button
           
              className="btn btn-success btn-xs" Width="95px"
              style={{ margin: "2px" }}
          >
              Approve
          </button>
        

          <Link to={`/viewpayer/${row.payer_id}`}>
                  <button
                      className="btn btn-danger btn-xs" Width="95px"
                      style={{ margin: "2px" }}
                   //   onClick={() => getIdPayerList(row.payer_id)}
                  >
                     Rejected
                  </button>

              </Link>
              </div>
          

        );
    },
},
  
]

  const pagination = paginationFactory({
    page: 1,
    sizePerPage: 5,
    lastPageText: '>>',
    firstPageText: '<<',
    nextPageText: '>',
    prePageText: '<',
    showTotal: true,
    alwaysShowAllBtns: true,
    onPageChange: function (page, sizePerPage) {
        console.log('page', page)
        console.log('sizePerPage', sizePerPage)
    },
    onSizePerPageChange: function (page, sizePerPage) {
        console.log('page', page)
        console.log('sizePerPage', sizePerPage)
    }
})

  useEffect(() => {
    axios
      .get(`http://localhost:7000/api/organization/getOrganisationByViewId/${value}`)
      .then((res) => {
      
        setUserList(res.data.data);
        console.log(res.data.data,"ppppppppp")
      });
  },
  []);



  return (
    <Container fluid="md">
          <Card border="primary">
            {/* <Card.Header
              as="h5"
              style={{ backgroundColor: "blue", color: "#ffffff" }}
            >
              Payer
            </Card.Header> */}
              <h3 className='text-center mb-3 heading'>Payer  Details</h3>
                        <center><hr className='heading_line2'></hr></center>
                        <br></br>
            <Card.Body>
    
              <Row>
                <Col>
                </Col>
              </Row>
             <div className="container-fluid p-3">
    <BootstrapTable bootstrap4 keyField='id'
        columns={columns}
        data={userList}
        pagination={pagination}
       
    />
</div> 
               {/* <table class="table table-sm mt-3">
                <thead class="thead-dark">
                  <th>Name</th>
                  <th>Email</th>
                  <th>Payer Url</th>
                  
                </thead>
                <tbody>

                  {userList.map(x => <tr>
                    <td>{x.name}</td>
                    <td>{x.email}</td>
                    <td>{x.payer_url}</td>
                  
                   
    
                  </tr>)}
                  {userList.length == 0 && <tr>
                    <td className="text-center" colSpan="4">
                      <b>No data found to display.</b>
                    </td>
                  </tr>}
                </tbody>
              </table>  */}
    
             <Row>
                <Col md={2} style={{ width: "50%", textAlign: "left" }}>
                  <Form.Group controlId="pcform.payercontact">
                  <Link to={`/addPayerContact`}>
                    <Button variant="success">Payer Contact</Button>{" "}
                    </Link>
                  </Form.Group>
                
                </Col>
                <Col md={2} style={{ width: "50%", textAlign: "right" }}>
                  <Form.Group controlId="pcform.request">
                  <Link to={`/ticketRequest`}>
                    <Button variant="danger" style={{ marginLeft:"500%" }}>Request</Button>{" "}
                    </Link>
                  </Form.Group>
                </Col>
              </Row>
              <Row style={{ marginTop: "5px" }}>
                <Col md={12}>
                  <Card style={{ marginTop: "12px" }}>
                    <Card.Header
                      style={{ backgroundColor: "#000000", color: "#ffffff" }}
                    >
                      <i class="fa fa-document"></i> Documentation
                    </Card.Header>
                    <Card.Body>
                      <Row>
                        <Col md={6}>
                          <Card bg="primary" style={{ margin: "6px", textAlign: "left" }}>
                             {userList.map(user => {
                        const { new_app } = user;
                        return (
                          <div >
                             <Card.Body>
                             <Card.Title>How to request for a new App</Card.Title>
                             <Card.Text>{new_app}</Card.Text>
                             </Card.Body>
                          </div>

                        )

                      })
                      }
                          </Card>
                        </Col>
                        <Col md={6}>
                          <Card bg="secondary" style={{ margin: "6px", textAlign: "left" }}>
                             {userList.map(user => {
                        const { view_existing_app } = user;
                        return (
                          <div >
                             <Card.Body>
                             <Card.Title>How to view existing App</Card.Title>
                             <Card.Text>{view_existing_app}</Card.Text>
                             </Card.Body>
                          </div>

                        )

                      })
                      }
                          </Card>
                        </Col>
                        <Col md={6}>
                          <Card bg="warning" style={{ margin: "6px", textAlign: "left" }}>
                                       {userList.map(user => {
                        const { delete_app } = user;
                        return (
                          <div >
                             <Card.Body>
                             <Card.Title>How to delete App</Card.Title>
                             <Card.Text>{delete_app}</Card.Text>
                             </Card.Body>
                          </div>

                        )

                      })
                      }
                          </Card>
                        </Col>
                        <Col md={6}>
                          <Card bg="success" style={{ margin: "6px", textAlign: "left" }}>
                      
                          
                              {userList.map(user => {
                        const { patient_access_api } = user;
                        return (
                          <div >
                             <Card.Body>
                             <Card.Title>Patient Access API End Point details</Card.Title>
                             <Card.Text>{patient_access_api}</Card.Text>
                             </Card.Body>
                          </div>

                        )

                      })
                      }
                          </Card>
                        </Col>
                        <Col md={6}>
                          <Card bg="light" style={{ margin: "6px", textAlign: "left" }}>
                         
                            {userList.map(user => {
                        const { provider_access_api } = user;
                        return (
                          <div >
                             <Card.Body>
                             <Card.Title>Provider Access API End Point details</Card.Title>
                             <Card.Text>{provider_access_api}</Card.Text>
                             </Card.Body>
                          </div>

                        )

                      })
                      }
                          </Card>
                        </Col>
                      </Row>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Container>



  );
  
}

export default ViewPayer





// const ViewPayer = () => {


  
  
 
 
//   const [viewpayerList, setViewPayerList] = useState([]);
//   const [viewpayer, setViewPayer] = useState([]);

//   useEffect(() => {
//     getIdPayerList()
//     //getPayerList()
   
//   }, []);



// //https://github.com/bezkoder/react-fetch-example/blob/master/src/App.js
// //https://www.codingdeft.com/posts/react-fetch-data-api/
//   function getPayerList() {
//     axios.get('http://localhost:7000/api/getAllrequestedpayers').then(res => {
//       setViewPayerList(res.data.data);
//       console.log(res.data.data, "padma")
//     });
//   }

//   const { id } = useParams();
//   function getIdPayerList  ()  {

//     fetch(`http://localhost:7000/api/getIdrequestedpayers/${id}`, {
//         method: 'get', 

//     }).then((res) => {
//       setViewPayerList(res.data.data);
//       console.log(res.data.data, "ppppppppppppppp")
//     })
//     .catch(error => { 
       
//         console.log(error.data) });

// }

//   return (
//     <Container fluid="md">
//       <Card border="primary">
//         <Card.Header
//           as="h5"
//           style={{ backgroundColor: "blue", color: "#ffffff" }}
//         >
//           Payer
//         </Card.Header>
//         <Card.Body>

//           <Row>
//             <Col>
//             </Col>
//           </Row>
//           <table class="table table-sm mt-3">
//             <thead class="thead-dark">
//               <th>Name</th>
//               <th>Email</th>
//               <th>Payer Url</th>


//             </thead>
//             <tbody>
              
//               {viewpayerList.map(x => <tr>
//                 <td>{x.name}</td>
//                 <td>{x.email}</td>
//                 <td>{x.payer_url}</td>

//               </tr>)}
//               {viewpayerList.length == 0 && <tr>
//                 <td className="text-center" colSpan="4">
//                   <b>No data found to display.</b>
//                 </td>
//               </tr>}
//             </tbody>
//           </table>

//           <Row>
//             <Col md={2} style={{ width: "50%", textAlign: "left" }}>
//               <Form.Group controlId="pcform.payercontact">
//               <Link to={`/addPayerContact`}>
//                 <Button variant="success">Payer Contact</Button>{" "}
//                 </Link>
//               </Form.Group>
            
//             </Col>
//             <Col md={2} style={{ width: "50%", textAlign: "right" }}>
//               <Form.Group controlId="pcform.request">
//                 <Button variant="danger">Request</Button>{" "}
//               </Form.Group>
//             </Col>
//           </Row>
//           <Row style={{ marginTop: "5px" }}>
//             <Col md={12}>
//               <Card style={{ marginTop: "12px" }}>
//                 <Card.Header
//                   style={{ backgroundColor: "#000000", color: "#ffffff" }}
//                 >
//                   <i class="fa fa-document"></i> Documentation
//                 </Card.Header>
//                 <Card.Body>
//                   <Row>
//                     <Col md={6}>
//                       <Card bg="primary" style={{ margin: "6px", textAlign: "left" }}>
//                         <Card.Body>
//                           <Card.Title>How to request for a new App</Card.Title>
//                           <Card.Text>
//                             With supporting text below as a natural lead-in to
//                             additional Lorem ipsum represents a long-held
//                             tradition for designers, typographers and the like.
//                             Some people hate it and argue for its demise.
//                           </Card.Text>
//                         </Card.Body>
//                       </Card>
//                     </Col>
//                     <Col md={6}>
//                       <Card bg="secondary" style={{ margin: "6px", textAlign: "left" }}>
//                         <Card.Body>
//                           <Card.Title>How to view existing App</Card.Title>
//                           <Card.Text>
//                             With supporting text below as a natural lead-in to
//                             additional Lorem ipsum represents a long-held
//                             tradition for designers, typographers and the like.
//                             Some people hate it and argue for its demise.
//                           </Card.Text>
//                         </Card.Body>
//                       </Card>
//                     </Col>
//                     <Col md={6}>
//                       <Card bg="warning" style={{ margin: "6px", textAlign: "left" }}>
//                         <Card.Body>
//                           <Card.Title>How to delete App</Card.Title>
//                           <Card.Text>
//                             With supporting text below as a natural lead-in to
//                             additional Lorem ipsum represents a long-held
//                             tradition for designers, typographers and the like.
//                             Some people hate it and argue for its demise.
//                           </Card.Text>
//                         </Card.Body>
//                       </Card>
//                     </Col>
//                     <Col md={6}>
//                       <Card bg="success" style={{ margin: "6px", textAlign: "left" }}>
//                         <Card.Body>
//                           <Card.Title>
//                             Patient Access API End Point details
//                           </Card.Title>
//                           <Card.Text>
//                             With supporting text below as a natural lead-in to
//                             additional Lorem ipsum represents a long-held
//                             tradition for designers, typographers and the like.
//                             Some people hate it and argue for its demise.
//                           </Card.Text>
//                         </Card.Body>
//                       </Card>
//                     </Col>
//                     <Col md={6}>
//                       <Card bg="light" style={{ margin: "6px", textAlign: "left" }}>
//                         <Card.Body>
//                           <Card.Title>
//                             Provider Access API End Point details
//                           </Card.Title>
//                           <Card.Text>
//                             With supporting text below as a natural lead-in to
//                             additional Lorem ipsum represents a long-held
//                             tradition for designers, typographers and the like.
//                             Some people hate it and argue for its demise.
//                           </Card.Text>
//                         </Card.Body>
//                       </Card>
//                     </Col>
//                   </Row>
//                 </Card.Body>
//               </Card>
//             </Col>
//           </Row>
//         </Card.Body>
//       </Card>
//     </Container>
//   );
// };

// export default ViewPayer;
