import React, { Component, useState, useEffect } from 'react';
//import '../css/Form.css';
import { Container, Row, Col, Form, Button, ExploreButton, Card } from 'react-bootstrap';
import { set, useForm } from "react-hook-form";
import axios from 'axios';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { propTypes } from 'react-bootstrap/esm/Image';
import { errorPrefix } from '@firebase/util';
import swal from 'sweetalert';


const btn = {
    "width": "20%",
    "textAlign": "center",
    "marginLeft": "40%",
}

const col = {
    "margin": "6px"
};

const sheader = {
    "backgroundColor": "blue",
    "color": "#ffffff"
}

const sheader_inner1 = {
    "backgroundColor": "gray",
    "color": "white"
}



let ResetPassword = () => {


    const { register, handleSubmit,reset, formState: { errors } } = useForm();
    const [errorMessage, setErrorMessage] = React.useState("");
    const [successMessage, setSuccessMessage] = React.useState("");

    const onSubmit = data => {
        console.log(data,"data")
        axios
            .post(
                'http://localhost:7000/api/user/resetPassword',
                data,
                //   { headers: { 'Content-Type': 'application/json' }}
            )
            .then(response => {
                setSuccessMessage("The reset password link has been sent to your email address!")
                reset(response.data);
                setTimeout(() => {
                    setSuccessMessage()
                  }, 2000);
                 console.log(response.data,"data") })
            .catch(error => { 
                setErrorMessage("Email does not exits")
                reset(error.data);
                setTimeout(() => {
                    setErrorMessage()
                  }, 2000);
                console.log(error.data) });
        
    };

    return (
        
        <Container fluid="md">

            <Card border="primary"
              style={{ width: "50%", align: "center", marginLeft: "25%" }}>
                <h3 className='text-center mb-3 heading'>Reset Password</h3>
                        <center><hr className='heading_line'></hr></center>
                <Card.Body>
                    <form onSubmit={handleSubmit(onSubmit)}>
                       
                        <Form.Group className="mb-3" controlId="pcform.password">
                            <Form.Label>Password</Form.Label>
                            <input className="form-control"
                               
                             {...register("password", {required: true } )}
                            />
                            {errors.password &&  <h4 style={{color:"red"}}>*</h4>}
    			
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="pcform.email">
                            <Form.Label> Confirm Password</Form.Label>
                            <input className="form-control"
                               
                             {...register("passwords", {required: true } )}
                            />
                            {errors.passwords &&  <h4 style={{color:"red"}}>*</h4>}
    				
                        </Form.Group>
                     
                        <Form.Group className="mb-3" controlId="pcform.submit">
                            <input type="submit" className="form-control btn btn-primary" style={{ width: "20%", marginLeft: "40%" }} />
                        </Form.Group>
                    </form>
                    <strong> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </strong>
             <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red" }} > {errorMessage}</div>} </strong>

                </Card.Body>
              
            </Card>
        </Container>
    )

}
export default ResetPassword;

// 

