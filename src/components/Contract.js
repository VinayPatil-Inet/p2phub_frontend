import React, { Component, useState, useEffect } from 'react';
import './css/Form.css'; //Import here your file style
import { Container, Row, Col, Form, Button, Card, ListGroup } from 'react-bootstrap';
import axios from 'axios';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useLocation } from "react-router-dom";

const btn = {
  "width": "20%",
  "textAlign": "center",
  "marginLeft": "40%",
}

const col = {
  "margin": "6px"
};

const sheader = {
  "backgroundColor": "blue",
  "color": "#ffffff"
}

const sheader_inner1 = {
  "backgroundColor": "gray",
  "color": "white"
}
const state = {
  curTime: new Date().toLocaleDateString(),
}

const Contract =  props => { 
  const [contractList, setContractList] = useState([]);
  const { payer_id } = useParams();
  const location = useLocation();

  const history = useHistory();
  useEffect(() => {
    console.log(location.state.payer_signed_status_id,"location payer_id");
   
  },
  []);

  // function getContractUserOrgDetails (){
  //   axios
  //   .post(`http://localhost:7000/api/user/login}`)
  //   .then((res) => {
    
  //     setContractList(res.data.data);
  //   //  history.push('/Billing');
  //     console.log(res.data,"ppppppppp")
  //   });

  // }
  // getContractUserOrgDetails()

  const handleRoute = () =>{ 
    console.log(location.state.payer_signed_status_id,"location.state.payer_signed_status_id")
    var data={id:location.state.payer_signed_status_id,payer_status:'Signed'}
    axios
      .post(`http://localhost:7000/api/PayersSignedStatus/update`,data)
      .then((res) => {
      
        setContractList(res.data.data);
      //  history.push('/Billing');
     // console.log(state.payer_status);
        console.log(res.data,"ppppppppp")
      });
    history.push("/searchPayer");
  }


  const handleRouteReject = () =>{ 
    console.log(location.state.payer_signed_status_id,"location.state.payer_signed_status_id")
    var data={id:location.state.payer_signed_status_id,payer_status:'Rejected'}
    axios
      .post(`http://localhost:7000/api/PayersSignedStatus/update`,data)
      .then((res) => {
      
        setContractList(res.data.data);
      //  history.push('/Billing');
     // console.log(state.payer_status);
        console.log(res.data,"ppppppppp")
      });
    history.push("/searchPayer");
  }
  return (

    <Container fluid="md">
      <Card border="primary" style={{ width: "80%", align: "center", marginLeft: "10%" }}>
        {/* <Card.Header as="h5" style={{ textAlign: "center" }}>Contract</Card.Header> */}
        {/* <div class="card-header" style={{ backgroundColor: "#a9afb5", color: "black" }}>
        Contract Details
                        </div> */}
                        <h3 className='text-center mb-3 heading'>Contract Details</h3>
                        <center><hr className='heading_line2'></hr></center>
                        <br></br>
        <Card.Body>
          <Row>
            <Col>
              <sup style={{ width: "80%", align: "right", marginLeft: "85%" }}>Date : {state.curTime}</sup>
            </Col>

          </Row>
          <Row>
            <Col>
              {contractList.map(user => {
                const { name, email, org_address1,org_city,org_state,org_postal_code,org_phone } = user;
                return (
                  <div >
                    <p style={{ color: "#2E71FF" }}>{name}.</p>
                    {/* <p>Email: {email}</p> */}
                    <p>{org_address1},</p>
                    <p>{org_city},</p>
                     <p>{org_state},</p>
                     <p>{org_postal_code},</p>
                     <p>Phone :{org_phone}.</p>
                  
                  </div>

                )

              })
              }
            </Col>
            
            <Col>
            {contractList.map(user => {
                const { first_name,last_name, email, address1,city,state,phone ,zip_code} = user;
                return (
                  <div >
                    <p style={{ color: "#2E71FF" }}>{first_name} {last_name}.</p>
                    {/* <p>Email: {email}</p> */}
                    <p>{address1},</p>
                    <p>{city},</p>
                     <p>{state},</p>
                     <p>{zip_code},</p>
                     <p>Phone :{phone}.</p>
                  
                  </div>

                )

              })
              }
            </Col>
          </Row>

          
          <Row>
            <Col>
              <Card border="primary" style={{ width: "100%", align: "center" }}>
              <div class="card-header" style={{ backgroundColor: "#a9afb5", color: "#ffffff" }}>
              Terms and Condition
                        </div>
                {/* <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>Terms and Condition</Card.Header> */}
                <Card.Body>
                  <Row>
                    <Col>
                     {/*  {contractList.map(user => {
                        const { terms_and_condition } = user;
                        return (
                          <div >
                           <p>{terms_and_condition}</p> 
                          
                          </div>

                        )

                      })
                      }*/}
                      <p>This Terms of Service Agreement ("Agreement") is entered into by and between HealthChain, registered address bgm, India ("Company") and you, and is made effective as of the date of your use of this website http://www.test.com ("Site") or the date of electronic acceptance.</p>
<p>This Agreement sets forth the general terms and conditions of your use of the http://www.test.com as well as the products and/or services purchased or accessed through this Site (the "Services").Whether you are simply browsing or using this Site or purchase Services, your use of this Site and your electronic acceptance of this Agreement signifies that you have read, understand, acknowledge and agree to be bound by this Agreement our Privacy policy. The terms "we", "us" or "our" shall refer to Company. The terms "you", "your", "User" or "customer" shall refer to any individual or entity who accepts this Agreement, uses our Site, has access or uses the Services. Nothing in this Agreement shall be deemed to confer any third-party rights or benefits.</p>
<p>Company may, in its sole and absolute discretion, change or modify this Agreement, and any policies or agreements which are incorporated herein, at any time, and such changes or modifications shall be effective immediately upon posting to this Site. Your use of this Site or the Services after such changes or modifications have been made shall constitute your acceptance of this Agreement as last revised.</p>
<p>IF YOU DO NOT AGREE TO BE BOUND BY THIS AGREEMENT AS LAST REVISED, DO NOT USE (OR CONTINUE TO USE) THIS SITE OR THE SERVICES.</p>
                      
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
              <Row>
                <Card border="primary" style={{ width: "47%", margin: "1.5%", align: "left" }}>
                <div class="card-header" style={{ backgroundColor: "#a9afb5", color: "#ffffff" }}>
                The Agreement
                        </div>
                  {/* <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>The Agreement</Card.Header> */}
                  <Card.Body>
                    <Col>
                  {/*   {contractList.map(user => {
                        const { agreement } = user;
                        return (
                          <div >
                            <p>{agreement}</p>
                          </div>

                        )

                      })
                    }*/}
                    <p>This Site and the Services are available only to Users who can form legally binding contracts under applicable law. By using this Site or the Services, you represent and warrant that you are (i) at least eighteen (18) years of age, (ii) otherwise recognized as being able to form legally binding contracts under applicable law, and (iii) are not a person barred from purchasing or receiving the Services found under the laws of the India or other applicable jurisdiction.</p>
<p>If you are entering into this Agreement on behalf of a company or any corporate entity, you represent and warrant that you have the legal authority to bind such corporate entity to the terms and conditions contained in this Agreement, in which case the terms "you", "your", "User" or "customer" shall refer to such corporate entity. If, after your electronic acceptance of this Agreement, Company finds that you do not have the legal authority to bind such corporate entity, you will be personally responsible for the obligations contained in this Agreement.</p>
                         
                    </Col>
                  </Card.Body>
                </Card>
                <Card border="primary" style={{ width: "47%", margin: "1.5%", align: "right" }}>
                <div class="card-header" style={{ backgroundColor: "#a9afb5", color: "#ffffff" }}>
                Term of Agreement
                        </div>
                  {/* <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>Term of Agreement</Card.Header> */}
                  <Card.Body>
                    <Col>
                  {/*  {contractList.map(user => {
                        const { term_of_agreement } = user;
                        return (
                          <div >
                            <p>{term_of_agreement}</p>
                          </div>

                        )

                      })
                      }*/}
                      <p>By using this Site You acknowledge and agree that:
Your use of this Site, including any content you submit, will comply with this Agreement and all applicable local, state, national and international laws, rules and regulations.</p>
<p>You will not use this Site in a manner that:</p>
Is illegal, or promotes or encourages illegal activity;
<p><ul><li>Promotes, encourages or engages in child pornography or the exploitation of children;</li>
<li>Promotes, encourages or engages in terrorism, violence against people, animals, or property;</li>
<li>Promotes, encourages or engages in any spam or other unsolicited bulk email, or computer or network hacking or cracking;</li>
<li>Infringes on the intellectual property rights of another User or any other person or entity;</li>
<li>Violates the privacy or publicity rights of another User or any other person or entity, or breaches any duty of confidentiality that you owe to another User or any other person or entity;</li>
</ul></p>
                    </Col>
                  </Card.Body>
                </Card>
              </Row>
              <Row>
               {/*  <Card border="primary" style={{ width: "47%", margin: "1.5%", align: "left" }}>
                <div class="card-header" style={{ backgroundColor: "blue", color: "#ffffff" }}>
                Non-Compete
                        </div>*/}
                  {/* <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>Non-Compete</Card.Header> */}
                 {/*  <Card.Body>
                    <Col>
                    {contractList.map(user => {
                        const { non_compete } = user;
                        return (
                          <div >
                            <p>{non_compete}</p>
                          </div>

                        )

                      })
                      }
                  
                    </Col>
                  </Card.Body>
                </Card>
                <Card border="primary" style={{ width: "47%", margin: "1.5%", align: "right" }}>
                <div class="card-header" style={{ backgroundColor: "blue", color: "#ffffff" }}>
                Non-Solicitation
                        </div>*/}
                  {/* <Card.Header as="h5" style={{ backgroundColor: "blue", color: "#ffffff" }}>Non-Solicitation</Card.Header> */}
                  {/* <Card.Body>
                    <Col>
                    {contractList.map(user => {
                        const { non_solicitation } = user;
                        return (
                          <div >
                            <p>{non_solicitation}</p>
                          </div>

                        )

                      })
                      }
                  
                    </Col>
                  </Card.Body>
                </Card>*/}
              </Row>
            </Col>
          </Row>
          <Row>

            <Col>
              <div class="col-md-12 text-center">
                {/* <Button    onClick={handleRoute} variant="primary">SUBMIT</Button>{' '} */}
                <Button    onClick={handleRoute} variant="success">Accept</Button>{' '}
                <Button    onClick={handleRouteReject} variant="danger">Reject</Button>{' '}
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container >
  );
}

export default Contract;